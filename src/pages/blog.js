const Home = () => {
    const listings = [
      {
        name: 'blog 1',
        url: 'images/blog1.jpg',
        price: 100,
        summary: 'summary 1',
      },
  
      {
        name: 'blog 2',
        url: 'images/blog2.jpg',
        price: 200,
        summary: 'summary 2',
      },
  
      {
        name: 'blog 3',
        url: 'images/blog3.jpg',
        price: 300,
        summary: 'summary 3',
      },
  
      {
        name: 'blog 4',
        url: 'images/blog4.jpg',
        price: 400,
        summary: 'summary 4',
      },
  
    ]
  
    return (
      <div className='container'>
        <div style={{}} className='row'>
          {listings.map((listing) => {
            return (
              <div
                className='card col'
                style={{
                  margin: 20,
                  display: 'inline-block',
                  cursor: 'pointer',
                }}>
                <img style={{ height: 200, display: 'block' }} src={{}} />
                <div className='card-body'>
                  <h5 className='card-title'>{listing.name}</h5>
                  <p className='card-text'>{listing.summary}</p>
                </div>
              </div>
            )
          })}
        </div>
      </div>
    )
  }
  
  export default Home
  